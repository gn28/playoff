<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'standings',
    'uses' => 'StandingsController@index'
]);

Route::post('/play', [
    'as' => 'standings.play',
    'uses' => 'StandingsController@play'
]);

