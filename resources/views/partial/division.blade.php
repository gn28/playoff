<table class="table table-bordered">
    <thead>
    <tr>
        <th class="col-sm-1" scope="col">Teams</th>
        @foreach ($teams as $team)
            <th class="col-sm-1" scope="col">{{ $team->name }}</th>
        @endforeach
        <th class="col-sm-1" scope="col">Score</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($teams as $index => $team)
        <tr>
            <th scope="row">{{$team->name}}</th>
            @if ($team->games()->count())
                @foreach ($team->games() as $key => $game)
                    @if ($index == $key)
                        <td class="table-inactive"></td>
                    @endif

                    @if ($index <= $key )
                        @if ($team->id == $game->winner_team_id )
                            <td>{{$game->winner_score}}-{{$game->loser_score}}</td>
                        @else
                            <td>{{$game->loser_score}}-{{$game->winner_score}}</td>
                        @endif
                    @else
                        @if ($team->id == $game->winner_team_id )
                            <td>{{$game->loser_score}}-{{$game->winner_score}}</td>
                        @else
                            <td>{{$game->winner_score}}-{{$game->loser_score}}</td>
                        @endif
                    @endif
                @endforeach

                @if ($loop->last)
                    <td class="table-inactive"></td>
                @endif
            @else
                @for ( $i = 0; $i < 8; $i++ )
                    <td class="{{ $i == $index ? "table-secondary" : ''}}"></td>
                @endfor
            @endif

            <td class="score">{{ $team->winnerQualifications->count() ? $team->winnerQualifications->count() : '' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
