<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Standings</title>

    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script
        src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
        crossorigin="anonymous"></script>
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 64px;
        }
        .division-title {
            font-size: 24px;
            float: left;
        }
        .play-off {
            margin-top: 12px
        }
        .standing-list {
            font-size: 12px;
            font-weight: bold;
        }
        .score {
            font-weight: bold;
        }
        #play {
            background: rgb(244,244,244);
            height: 76px;
            width: 120px;
            font-weight: bold;
            color: #636b6f;
        }
        .table-inactive {
            background-color: rgb(244,244,244)!important;
        }
    </style>
</head>
<body>
<div class="content">
    <div class="title">
        Standings
    </div>

    <div class="container tables">
        <div class="row">
            <div class="col">
                <div class="division-title">
                    Division A
                </div>
                @include('partial.division', ['teams' => $teamsA])
            </div>
            <div class="col">
                <div class="division-title">
                    Division B
                </div>
                @include('partial.division', ['teams' => $teamsB])
            </div>
        </div>

        <div class="row play-off">
            <div class="col">
                <div class="division-title">
                    Play-off
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Quarter-final</th>
                        <th scope="col">Semi-final</th>
                        <th scope="col">Final</th>
                        <th scope="col">Result</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($playoff->count())
                        <tr>
                            <td class="col-sm-3 align-middle">
                                {{$topA[0]['name']}} (Division A) / {{$topB[3]['name']}} (Division B)
                                @if ($playoff->quarterWinnerId(1) == $topA[0]['id'])
                                    <span class="score">{{$playoff->quarterWinnerScore(1) }}:{{$playoff->quarterLoserScore(1)}}</span>
                                @else
                                    <span class="score">{{$playoff->quarterLoserScore(1)}}:{{$playoff->quarterWinnerScore(1)}}</span>
                                @endif
                            </td>
                            <td class="col-sm-3 align-middle" rowspan="2">
                                {{$playoff->semiWinnerName(1)}} / {{$playoff->semiLoserName(1)}}
                                <span class="score">{{$playoff->semiWinnerScore(1)}}:{{$playoff->semiLoserScore(1)}}</span>
                            </td>
                            <td class="col-sm-3 align-middle" rowspan="2">
                                {{$playoff->finalWinnerName()}} / {{$playoff->finalLoserName()}}
                                <span class="score">{{$playoff->finalWinnerScore()}}:{{$playoff->finalLoserScore()}}</span>
                            </td>
                            <td class="col-sm-2 standing-list" rowspan="4">
                                <ol>
                                    <li> {{$playoff->finalWinnerName()}}</li>
                                    <li> {{$playoff->finalLoserName()}}</li>
                                    <li> {{$playoff->thirdWinnerName()}}</li>
                                    <li> {{$playoff->thirdLoserName()}}</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <td class="align-middle">
                                {{$topA[1]['name']}} (Division A) / {{$topB[2]['name']}} (Division B)
                                    @if ($playoff->quarterWinnerId(2) == $topA[1]['id'])
                                        <span class="score">{{$playoff->quarterWinnerScore(2) }}:{{$playoff->quarterLoserScore(2)}}</span>
                                    @else
                                        <span class="score">{{$playoff->quarterLoserScore(2)}}:{{$playoff->quarterWinnerScore(2)}}</span>
                                    @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="align-middle">
                                {{$topA[2]['name']}} (Division A) / {{$topB[1]['name']}} (Division B)
                                    @if ($playoff->quarterWinnerId(3) == $topA[2]['id'])
                                        <span class="score">{{$playoff->quarterWinnerScore(3) }}:{{$playoff->quarterLoserScore(3)}}</span>
                                    @else
                                        <span class="score">{{$playoff->quarterLoserScore(3)}}:{{$playoff->quarterWinnerScore(3)}}</span>
                                    @endif
                            </td>
                            <td class="align-middle" rowspan="2">
                                {{$playoff->semiWinnerName(2)}} / {{$playoff->semiLoserName(2)}}
                                <span class="score">{{$playoff->semiWinnerScore(2)}}:{{$playoff->semiLoserScore(2)}}</span>
                            </td>
                            <td class="align-middle" rowspan="2">
                                {{$playoff->thirdWinnerName()}} / {{$playoff->thirdLoserName()}}
                                <span class="score">{{$playoff->thirdWinnerScore()}}:{{$playoff->thirdLoserScore()}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="align-middle">
                                {{$topA[3]['name']}} (Division A) / {{$topB[0]['name']}} (Division B)
                                    @if ($playoff->quarterWinnerId(4) == $topA[3]['id'])
                                        <span class="score">{{$playoff->quarterWinnerScore(4) }}:{{$playoff->quarterLoserScore(4)}}</span>
                                    @else
                                        <span class="score">{{$playoff->quarterLoserScore(4)}}:{{$playoff->quarterWinnerScore(4)}}</span>
                                    @endif
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td class="col-sm-3 align-middle"> Team (Division A) / Team (Division B)
                            </td>
                            <td class="col-sm-3 align-middle" rowspan="2"></td>
                            <td class="col-sm-3 align-middle" rowspan="2">
                            </td>
                            <td class="col-sm-2 standing-list text-left" rowspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td class="align-middle">Team (Division A) / Team (Division B)
                            </td>
                        </tr>
                        <tr>
                            <td class="align-middle">Team (Division A) / Team (Division B)
                            </td>
                            <td class="align-middle" rowspan="2">
                            </td>
                            <td class="align-middle" rowspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="align-middle">
                                Team (Division A) / Team (Division B)
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        <button id="play" class="btn btn-play">
            Play
        </button>
    </div>
</div>
</body>
</html>
<script>
    $(document).on('click', '#play', function(){
        $(this).empty();
        $(this).append('<img src="/images/loading.gif">');
        $.ajax({
            url: "/play",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            type: "post",
            dataType: "json",
            success: function(response) {
                console.log(response);
                location.reload();
            }
        });
    })
</script>
