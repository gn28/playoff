<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playoff extends Model
{
    protected $table = 'playoffs';

    protected $guarded = [];

    public function winner() {
        return $this->belongsTo(Team::class, 'winner_id', 'id');
    }

    public function loser() {
        return $this->belongsTo(Team::class, 'loser_id', 'id');
    }

    public function quarterWinnerId(int $order) : int {
        return $this->findOrFail($order)->winner_id;
    }

    public function quarterWinnerScore(int $order) : int {
        return $this->findOrFail($order)->winner_score;
    }

    public function quarterLoserScore(int $order) : int {
        return $this->findOrFail($order)->loser_score;
    }

    public function semiWinnerName(int $order) : string {
        return $this->find(4 + $order)->winner->name;
    }

    public function semiLoserName(int $order) : string {
        return $this->find(4 + $order)->loser->name;
    }

    public function semiWinnerScore(int $order) : int {
        return $this->find(4 + $order)->winner_score;
    }

    public function semiLoserScore(int $order) : int {
        return $this->find(4 + $order)->loser_score;
    }

    public function finalWinnerScore() : int {
        return $this->where('type', 'final')->first()->winner_score;
    }

    public function finalLoserScore() : int {
        return $this->where('type', 'final')->first()->loser_score;
    }

    public function thirdWinnerScore() : int {
        return $this->where('type', 'third')->first()->winner_score;
    }

    public function thirdLoserScore() : int {
        return $this->where('type', 'third')->first()->loser_score;
    }

    public function finalWinnerName() : string {
        return $this->where('type', 'final')->first()->winner->name;
    }

    public function finalLoserName() : string {
        return $this->where('type', 'final')->first()->loser->name;
    }

    public function thirdWinnerName() : string {
        return $this->where('type', 'third')->first()->winner->name;
    }

    public function thirdLoserName() : string {
        return $this->where('type', 'third')->first()->loser->name;
    }

}
