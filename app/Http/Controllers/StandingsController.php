<?php

namespace App\Http\Controllers;

use App\Playoff;
use App\Team;
use Illuminate\Support\Facades\Artisan;

class StandingsController extends Controller
{
    public function index() {
        $teamsA = Team::where('division', 'A')->with('winnerQualifications')->get();
        $teamsB = Team::where('division', 'B')->with('winnerQualifications')->get();

        $teamObj = new Team;
        $topA = $teamObj->top('A');
        $topB = $teamObj->top('B');

        $playoff = new Playoff;

        return view('standings.index', [
            'teamsA' => $teamsA,
            'teamsB' => $teamsB,
            'topA' => $topA,
            'topB' => $topB,
            'playoff' => $playoff
        ]);
    }

    public function play() {
        Artisan::queue('standings:play');

        return response()->json(['success' => true]);
    }

}
