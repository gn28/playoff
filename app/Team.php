<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';

    protected $guarded = [];

    public function winnerQualifications() {
        return $this->hasMany(Qualification::class, 'winner_team_id', 'id');
    }

    public function loserQualifications() {
        return $this->hasMany(Qualification::class, 'loser_team_id', 'id');
    }

    public function games() {
        return Qualification::where('loser_team_id', $this->id)
            ->orWhere('winner_team_id', $this->id)
            ->get();
    }

    public function top(string $division) {
        return array_values( $this->where('division', $division)
            ->get()
            ->sortByDesc('winning_percentage')
            ->sortByDesc( function($query) {
                return $query->winnerQualifications->count();
            })
            ->take(4)
            ->toArray() );
    }
}
