<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Team;
use App\Qualification;
use App\Playoff;

class PlayCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'standings:play';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Qualification::truncate();
        Playoff::truncate();

        $divisionA = Team::where('division', 'A')->get();
        $divisionB = Team::where('division', 'B')->get();

        $this->playGames($divisionA);
        $this->playGames($divisionB);

        $this->playoff();
    }

    private function playoff() {
        $teamObj = new Team();
        $topA = $teamObj->top('A');
        $topB = $teamObj->top('B');

        foreach ($topA as $index => $teamA) {
            $firstScore = random_int(0, 8);
            $secondScore = random_int(0, 8);
            if ($firstScore == $secondScore) {
                $pointTeam = random_int(0, 1);
                if ($pointTeam) {
                    $firstScore++;
                } else {
                    $secondScore++;
                }
            }
            $this->playoffGame($firstScore, $secondScore, $teamA['id'], $topB[ 3 - $index ]['id'], 'quarter');
        }

        for ($i = 0; $i < 4; $i++) {
            $firstScore = random_int(0, 8);
            $secondScore = random_int(0, 8);
            if ($firstScore == $secondScore) {
                $pointTeam = random_int(0, 1);
                if ($pointTeam) {
                    $firstScore++;
                } else {
                    $secondScore++;
                }
            }

            if ($i < 2) {
                $this->playoffGame($firstScore, $secondScore, Playoff::find(1 + ($i ? 2 : 0))->winner_id, Playoff::find(2 + ($i ? 2 : 0))->winner_id, 'semi');
            }

            if ($i == 2) {
                $this->playoffGame($firstScore, $secondScore, Playoff::find(5)->loser_id, Playoff::find(6)->loser_id, 'third');
            }

            if ($i == 3) {
                $this->playoffGame($firstScore, $secondScore, Playoff::find(5)->winner_id, Playoff::find(6)->winner_id, 'final');
            }

        }
    }

    private function playGames($division) : void {
        $teamNames = [];

        foreach($division as $teamFirst) {
            foreach($division as $teamSecond) {
                if ($teamFirst->name != $teamSecond->name && !in_array($teamFirst->id . $teamSecond->id , $teamNames)) {
                    $teamNames[] = $teamFirst->id . $teamSecond->id;
                    $teamNames[] = $teamSecond->id . $teamFirst->id;
                    $firstScore = random_int(0, 8);
                    $secondScore = random_int(0, 8);
                    if ($firstScore == $secondScore) {
                        $pointTeam = random_int(0, 1);
                        if ($pointTeam) {
                            $firstScore++;
                        } else {
                            $secondScore++;
                        }
                    }
                    if ($firstScore > $secondScore) {
                        Qualification::create([
                            'winner_team_id' => $teamFirst->id,
                            'loser_team_id' => $teamSecond->id,
                            'winner_score' => $firstScore,
                            'loser_score' => $secondScore
                        ]);
                    } else {
                        Qualification::create([
                            'winner_team_id' => $teamSecond->id,
                            'loser_team_id' => $teamFirst->id,
                            'winner_score' => $secondScore,
                            'loser_score' => $firstScore
                        ]);
                    }
                }
            }
        }

    }

    private function playoffGame (int $firstScore, int $secondScore, int $firstTeam, int $secondTeam, string $type ) : void {
        if ($firstScore > $secondScore) {
            Playoff::create([
                'type' => $type,
                'winner_id' => $firstTeam,
                'loser_id' => $secondTeam,
                'winner_score' => $firstScore,
                'loser_score' => $secondScore,
            ]);
        } else {
            Playoff::create([
                'type' => $type,
                'winner_id' => $secondTeam,
                'loser_id' => $firstTeam,
                'winner_score' => $secondScore,
                'loser_score' => $firstScore,
            ]);
        }
    }

}
