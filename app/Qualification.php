<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $table = 'qualifications';

    protected $guarded = [];

    public function winner() {
        return $this->belongsTo(Team::class, 'winner_team_id', 'id');
    }

    public function loser() {
        return $this->belongsTo(Team::class, 'loser_team_id', 'id');
    }

}
