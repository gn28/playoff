<?php

use Illuminate\Database\Seeder;
use App\Team;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = [
            [
                'name' => 'A',
                'division' => 'A',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'B',
                'division' => 'A',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'C',
                'division' => 'A',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'D',
                'division' => 'A',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'E',
                'division' => 'A',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'F',
                'division' => 'A',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'G',
                'division' => 'A',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'H',
                'division' => 'A',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'I',
                'division' => 'B',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'J',
                'division' => 'B',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'K',
                'division' => 'B',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'L',
                'division' => 'B',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'N',
                'division' => 'B',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'M',
                'division' => 'B',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'O',
                'division' => 'B',
                'winning_percentage' => $this->winningPercantage()
            ],
            [
                'name' => 'P',
                'division' => 'B',
                'winning_percentage' => $this->winningPercantage()
            ]
        ];

        foreach ($teams as $team) {
            Team::create($team);
        }
    }

    private function winningPercantage() {
       return random_int(0, 99) + (float)rand() / (float)getrandmax();
    }
}
