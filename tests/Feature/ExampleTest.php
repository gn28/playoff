<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_making_a_play_request()
    {
        $response = $this->postJson('/play');

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
